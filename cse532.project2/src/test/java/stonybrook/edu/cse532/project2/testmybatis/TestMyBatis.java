package stonybrook.edu.cse532.project2.testmybatis;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;  
import org.springframework.context.support.ClassPathXmlApplicationContext;  
import org.springframework.test.context.ContextConfiguration;  
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import stonybrook.edu.cse532.project2.dao.Person;  


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-mybatis.xml"})
public class TestMyBatis {
	private static Logger logger = Logger.getLogger(TestMyBatis.class);
	@Resource
	private SqlSession m_SqlSession;
	
	@Test
	public void test1() {
		logger.info("into test1");
		List<Person> t_QueryResult = m_SqlSession.selectList("data-mapper.selectAllPeople");
		
		
		logger.info(t_QueryResult);
	}
}
