/**
 * 
 */
package stonybrook.edu.cse532.project2.entities;

/**
 * @author fan
 *
 */
public class Person {
	private int id;
	private String name;
	private Integer age;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	
	public String toString() {
		return this.id + "\t" + this.name + "\t" + this.age;
		
	}
}
