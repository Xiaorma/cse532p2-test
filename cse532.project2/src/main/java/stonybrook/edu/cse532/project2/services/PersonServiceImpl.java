package stonybrook.edu.cse532.project2.services;

import java.util.List;
import org.springframework.stereotype.Service;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import stonybrook.edu.cse532.project2.entities.Person;
@Service
public class PersonServiceImpl implements PersonService {
	@Autowired
	private SqlSession sqlSession;
	
	public List<Person> getAllPeople() {
		return sqlSession.selectList("stonybrook.edu.cse532.project2.mapper.PersonMapper.selectAllPeople");
	}

	public Person getOnePerson(int id) {
		return sqlSession.selectOne("stonybrook.edu.cse532.project2.mapper.PersonMapper.selectOnePerson", id);
	}

	public void updatePerson(Person p) {
		sqlSession.update("stonybrook.edu.cse532.project2.mapper.PersonMapper.updatePerson", p);
	}

	public void insertPerson(Person p) {
		sqlSession.insert("stonybrook.edu.cse532.project2.mapper.PersonMapper.insertPerson", p);
	}

	public void deletePerson(int id) {
		sqlSession.delete("stonybrook.edu.cse532.project2.mapper.PersonMapper.deletePerson", id);
	}

}
