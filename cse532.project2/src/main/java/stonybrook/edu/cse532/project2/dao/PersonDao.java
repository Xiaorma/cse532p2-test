package stonybrook.edu.cse532.project2.dao;

import java.util.List;

import stonybrook.edu.cse532.project2.entities.Person;

public interface PersonDao {
	public List<Person> selectAllPeople();
	
	public Person selectOnePerson(int id);
	
	public void insertPerson(Person p);
	
	public void updatePerson(Person p);
	
	public void deletePerson(int id);

}
