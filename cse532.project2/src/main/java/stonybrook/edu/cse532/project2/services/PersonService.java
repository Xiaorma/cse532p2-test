package stonybrook.edu.cse532.project2.services;

import java.util.List;

import stonybrook.edu.cse532.project2.entities.Person;

public interface PersonService {
	public List<Person> getAllPeople();
	public Person getOnePerson(int id);
	public void updatePerson(Person p);
	public void insertPerson(Person p);
	public void deletePerson(int id);
}
