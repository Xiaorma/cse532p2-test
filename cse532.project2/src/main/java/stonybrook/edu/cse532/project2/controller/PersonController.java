package stonybrook.edu.cse532.project2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import stonybrook.edu.cse532.project2.entities.Person;
import stonybrook.edu.cse532.project2.services.PersonService;

@Controller
@RequestMapping("/person")
public class PersonController {
	@Autowired
	private PersonService personServiceImpl;
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public @ResponseBody Person GetOnePerson(@PathVariable int id) {
		Person one = personServiceImpl.getOnePerson(id);
		System.out.println("Hello World");
		return one;
	}
}
